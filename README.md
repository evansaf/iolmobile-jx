### jx - Simplistic opinionated JSON for isomorphic HTML generation ###

```
$ git clone git@bitbucket.org:evansaf/iolmobile-jx.git
$ cd iolmobile-jx
$ npm install 
$ nodejs test/example.js
```

The "library" is `src/jx.js` which is 100-odd lines of code, so quite minimal.

```
evanx@faramir:~/iolmobile-jx$ cat src/jx.js | grep ';' | wc -l 
112
```

My employers have graciously allowed me to opensource this code, as it was developed in my own time.
